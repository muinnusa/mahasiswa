
//Nama class
Class Mahasiswa{
	//Deklarasi atribut
	private int Nim;
	private int Nama;
	//Method konstruktor
	public Mahasiswa(int i,String n){
		this.nim=i;
		this.nama=n;
	}
	//Method getNim
	public int getNim(){
		return nim;
	}
	//Method getNama
	public String getNama(){
		return nama;
	}
	//Method main
	public static void main(String[]args){
		Mahasiswa siswa=new Mahasiswa(12345,"JOKO");
		System.out.println("NIM : "+siswa.getnim());
		System.out.println("Nama : "+siswa.getnama());
	}
}